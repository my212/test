package ru.iteco.lot1.task1;
import ru.iteco.lot1.task1.common.WordMaps;
import ru.iteco.lot1.task1.thread.CalcFreq;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Достоинтва
 * 1. Обрабатывает также русский текст в файлах в кодировке UTF-8
 *
 * Общий алгоритм нахождения 10-и максимальных по частоте встерчаемости слов таков:
 * 1. Выбираем все файлы в каталоге и упорядочиваем по уменьшению размера таким образом чтобы сначала обрабатывались большие файлы
 * 2. Создаём число потоков обработки в соответствии с числом доступных процессорных ядер
 * 3. Каждый процесс обрабатывает один файл
 * 3.1 Файл считывается в строку. Строка разбивается на массив слов регулярным выражением
 * 3.2 Каждое слово из массива слов помещается в локальный словарь процесса где ключам служат слова (прописные буквы), а ключами - частота их встречаемости. Каждый раз когда слово встречается поворно, частота его встречаемости увеличивается на 1
 * 4. Когда процесс закончил работу, его локальный словарь потокобезопасно сливается с глобальным словарём
 * 5. Когда все процессы закончили работы, в глобальном словаре ищется 10-ь намиболее частов стречающихся слов
 */
public class JavaApplication {
    public static void main (String args [])  {
        int len=0;
        String catalog="";
        System.out.println("programm start");
        try{
            catalog=args[0];
            len=Integer.parseInt(args[1]);
        } catch (Exception e){
            System.out.println("Please enter input parameters. First parameter is directory, second parameter is the length.");
            System.out.println("Processed only .txt file encoded UTF-8");
            System.out.println("Example: task1 c:\\tmp\\ 6");
            return;
        }

        final File directory = new File(catalog);

        File[] files = directory.listFiles((File pathname) -> pathname.getName().endsWith("txt") && pathname.isFile());
        Arrays.sort(files, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.compare(f2.length(),f1.length());
            }
        });

        System.out.println("All txt-files found: "+files.length);

        ExecutorService executor = Executors.newWorkStealingPool(); //.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        System.out.println("Your processor count: "+Runtime.getRuntime().availableProcessors());
        List<Callable<Object>> todo = new ArrayList<Callable<Object>>();
        int finalLen = len;
        for(File f: files){
            todo.add(Executors.callable(new Runnable() {
                    public void run() {
                        try {
                            WordMaps.addWordsFrequencySynch(new CalcFreq(f, finalLen).execute());
                        }catch (IOException e){System.out.println("Error on processed file "+f.getName()); e.printStackTrace(); }
                    }
            }));
        }

        try{
            List<Future<Object>> answers = executor.invokeAll(todo);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(WordMaps.getTenMaxFrequency());
    }

   /* public static void main (String args []) throws IOException {
        //тестирование производительности

        File f = new File("c:\\Work\\OTHER\\beelaine\\beeline_lot1_task1\\catalog\\r4.txt");

        String content = Files.readString(f.toPath(), StandardCharsets.UTF_8);
        Pattern pattern = Pattern.compile("\\b"+"word"+"\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(content);

        long startTime = System.currentTimeMillis();
        for(int i=0; i<100; i++) {
            WordMaps.addWordsFrequencySynch(new CalcFreq(f, 2).execute());

            //matcher = pattern.matcher(content);
            //matcher.results().count();
            //content=matcher.replaceAll("");

            //WordInString ws=new CalcFreq(f, 2).getFirstWord(content);
        }
        long endTime = System.currentTimeMillis();

        System.out.println(endTime-startTime);
    }*/
}
