package ru.iteco.lot1.task1.common;

import ru.iteco.lot1.task1.thread.CalcFreq;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/** -------------------------------------------------------------
 * Класс аккамулирующий статистику по частоте встречающихся слов со всех потоков и предоставляющий функции для её обработки
 *  -------------------------------------------------------------
 */
public class WordMaps {
    /** Потокобезопасный словарь в котором ключами являются слова, а значениями их частота в текстовом файле */
    private static ConcurrentHashMap  <String,Long> mapWordFrequency = new ConcurrentHashMap<String,Long>();

    /**
     * Потокобезопасная (почему-то использования ConcurrentHashMap оказываетсянедостаточно) функция добавления словаря заполенного отдельным потоком подсчёта в общий словарь {@link WordMaps#mapWordFrequency}
     * @param newWord - словарь заполненный отдельным потоком подсчёта
     */
    public static synchronized  void addWordsFrequencySynch(Map <String,Long> newWord){
        for (Map.Entry<String, Long> item : newWord.entrySet()){
            Long oldFrequency=mapWordFrequency.get(item.getKey());
            if (!(oldFrequency instanceof Long)) oldFrequency=Long.valueOf(0);
            mapWordFrequency.put(item.getKey(),oldFrequency+item.getValue());
        }
    }
    /**
     * Функция поиска 10-и самых часто встречающихся слов в словаре {@link WordMaps#mapWordFrequency}
     * @return массив элементов класса  {@link WorldFrequency}
     */
    public static StringBuilder getTenMaxFrequency() {
        WorldFrequency wfArr[]=new WorldFrequency[10];

        /*long sum=(long)0;//!!
        for (Map.Entry<String, Long> item : mapWordFrequency.entrySet())
            sum+=item.getValue();*/

        for(int i=0; i<10; i++){
            WorldFrequency wf = new WorldFrequency();

            for (Map.Entry<String, Long> item : mapWordFrequency.entrySet())
                if (item.getValue() > wf.freq) {wf.freq=item.getValue();; wf.word=item.getKey();}

            if (wf.freq==-1) break;
            wfArr[i]=wf;
            mapWordFrequency.put(wf.word,Long.valueOf(-1));
        }

        StringBuilder sb=new StringBuilder();
        for(int i=0;i<10;i++) sb.append(wfArr[i].word+" - "+wfArr[i].freq+"\n");
        //sb.append(sum);//!!
        return sb;
    }
}

/**
 * Класс - запись содержащий слово и частоту его использования.
 */
class WorldFrequency{
    Long freq=Long.valueOf(0);
    String word="";
}
