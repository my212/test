package ru.iteco.lot1.task1.thread;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/** -------------------------------------------------------------
 * Класс реализующий интерфейс ICalculate и подсчитывающий число слов в одном текстовом файле (кодировка строго UTF-8)
 *  -------------------------------------------------------------
 */
public class CalcFreq implements ICalculate {
    /** текстовый файл в кодировке UTF-8*/
    private File f;
    /** Ищем только слова большей длинны чем эта */
    private int len;
    /** Словарь в котором ключами являются слова, а значениями их частота в текстовом файле */
    private Map<String,Long> mapWordFrequencyLocalFile = new HashMap<String,Long>();

    /**
     * Функция увеличения частоты встречаемости для конкретного слова в словаре {@link CalcFreq#mapWordFrequencyLocalFile}
     * @param word - слово строчными буквами
     */
    private void incWordFrequencyLocalFile(String word){
        Long oldFrequency=mapWordFrequencyLocalFile.get(word);
       if (!(oldFrequency instanceof Long)) oldFrequency=Long.valueOf(0);
        mapWordFrequencyLocalFile.put(word,oldFrequency+1);
    }

    /**
     * Конструктор - создание нового объекта с определенными значениями
     * @param f - текстовый файл в кодировке UTF-8
     * @param len - ищем только слова большей длинны чем эта
     */
    public CalcFreq(File f, int len){
        this.f=f; this.len=len;
    }

    /**
     * Функция подсчитвающая частоты слов и заполлняющая словарь {@link CalcFreq#mapWordFrequencyLocalFile}
     * @return заполненный словарь {@link CalcFreq#mapWordFrequencyLocalFile}
     */
    public Map<String,Long> execute() throws IOException {
        this.toLog("start processed file \""+f.getName()+"\" size "+f.length());

        for(String s : Files.readString(f.toPath(), StandardCharsets.UTF_8).split(this.regex))
            if (s.length()>len) this.incWordFrequencyLocalFile(s.toLowerCase());

        /*long sum=(long)0;//!!
        for (Map.Entry<String, Long> item : mapWordFrequencyLocalFile.entrySet())
            sum+=item.getValue();
        this.toLog("end processed file "+f.getName()+" :"+sum);*/

        this.toLog("end processed file "+f.getName());
        return mapWordFrequencyLocalFile;
    }
}
