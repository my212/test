package ru.iteco.lot1.task1.thread;

import java.io.IOException;
import java.util.Map;
/** -------------------------------------------------------------
 * ICalculate.java Декларация интерфейса для подсчёта числа слов в текстовом файле
 *  -------------------------------------------------------------
 */
public interface ICalculate {
    /** Регулярное выражение для разбиения строки на отдельные слова */
    String regex="\\P{L}+";//"[\\p{Punct}\\s]+"; //"\\s*(\\s|,|!|;|\uFEFF|?|\\.)\\s*";//"[^a-zA-Zа-яА-Я0-9ё-Ё]+";//"\\P{L}+";
    /**Вывод на консоль.
     * @param msg - текст сообщения
     * Потокобезопасность не требуется. Она уже есть в println
     */
    default void toLog(String msg){
        System.out.println(msg);
    }
    /**Запуск вычислений
     */
    Map<String,Long> execute() throws IOException;
}
