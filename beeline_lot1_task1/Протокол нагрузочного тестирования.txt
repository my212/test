22.04.2021

===����1. ���������� ������ ���������� ���������
���������� ��������: 100
����������� ���: WordMaps.addWordsFrequencySynch(new CalcFreq(f, 2).execute1());

��� �������: private String regex="\\P{L}+";
����� ���������� � ��: 7187, 7078, 7222

��� �������: private String regex="[^a-zA-Z�-��-�0-9]+";
����� ���������� � ��: 13656, 13969

��� �������: private String regex="\\s*(\\s|,|!|;|\uFEFF|\\.)\\s*";
����� ���������� � ��: 10000, 9922, 10453

��� �������: private String regex="[\\p{Punct}\\s]+";
����� ���������� � ��: 6281, 6422, 6531

�����: ������� ����
private String regex="[\\p{Punct}\\s]+";
private String regex="\\P{L}+";

===����2. ����������� ������ �� ������� execute1 � execute2. ������� ����� ���� � ���, ��� � ������ ������ .toLowerCase() �������� ��� ���� ������ (���������� �����) �����, � �� ������ .toLowerCase() �������� ��� ������� ����������� � ������� ����� ��������

���������� ��������: 100
��� �������: private String regex="[\\p{Punct}\\s]+";

����������� ���: WordMaps.addWordsFrequencySynch(new CalcFreq(f, 2).execute1());
����� ���������� � ��: 6578, 6485

����������� ���: WordMaps.addWordsFrequencySynch(new CalcFreq(f, 10).execute1());
����� ���������� � ��: 5094, 4875, 4813

����������� ���: WordMaps.addWordsFrequencySynch(new CalcFreq(f, 2).execute2());
����� ���������� � ��: 6203, 6031

����������� ���: WordMaps.addWordsFrequencySynch(new CalcFreq(f, 10).execute2());
����� ���������� � ��: 4000, 3969

�����:
1. execute2 �������� ���� ������� (�������� �� 10 - 15%)
2. ��� ������ ������ � ������� ����������, ��� ������� �������� (��� ����������� ��� ��� � ���� ������ ������� ���������� ��� �������� ����� � �������)

===����3. ��������� �������� ������ � pattern � matcher
���������� ��������: 100

����������� ���: 
	matcher.results().count();
	matcher.replaceAll("");
����� ���������� � ��: 4891, 4859

����������� ���: 
            matcher = pattern.matcher(content);
            matcher.results().count();
            content=matcher.replaceAll("");
����� ���������� � ��: 4906, 4844

�����: ������� ����� matcher ����������� ����������� ������� ����� split.
(��� ��� ����� 100 ��� ��������� - �� ���� ���������� ����� � 100 - 1000(���� �����������) ����, � �� ����������� ��� ����� ��� ��� ���������� 1,5 �� ����� ����� split)

����������� ���:
	new CalcFreq(f, 2).getFirstWord(content);
����� ���������� � ��: 15