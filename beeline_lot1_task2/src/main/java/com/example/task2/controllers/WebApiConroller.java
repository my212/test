package com.example.task2.controllers;

import com.example.task2.controllers.dto.CleaningDto;
import com.example.task2.controllers.dto.RecreateTestObjectDto;
import com.example.task2.controllers.dto.ResponseInfo;
import com.example.task2.db.service.*;
import com.example.task2.db.entity.DbLock;
import com.example.task2.db.entity.DbLog;
import com.example.task2.exception.InternalServerErrorException;
import com.example.task2.exec.Executor;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.UUID;

/**
 * Web Api контроллер. Позволяет:
 * 0. Автоматически пересоздавать (удалять созданные и создавать заново) объекты в БД и заполнять их тестовыми данными с настраиваемыми параметрами
 * 1. Просматривать последние записи в логах
 * 2. Запускать "очистку" таблиц от старых данных
 * 3. Просматривать статус запущенных процессов по очистке таблиц
 * В случае успеха возрващается http-статус 200. В случае неудачи - 500.
 */

/**
 * Общее описание алгоритма:
 * Логи ведутся в таблице в БД
 * При необходимости "очистить" таблицу - запускается отдельный процесс, который, с заданной периодичностью запускает хранимую процедуру выбирающую N-строк подходящих под параметры удаления и удаляющую их. После чего прцоесс запускает. Таким образом таблица в бД не блокируется на время очистки.
 * При начале работы, создаёт в таблице активных задач запись с именем очищаемой таблицы и другими параметрами. Таким образом одну таблицу, в один момент времени, может очищать только один процесс.
 * При завершении работы процесс удаляет созданную запись
 */
@RestController("api_v1")
public class WebApiConroller {
    private final DbLogService dbLogService;
    private final DbLockService dbLockService;
    private final DbCreateTestObjectServiceImpl dbCreateTestObjectService;
    private final DbTableServiceImpl dbTableService;

    @Autowired
    public WebApiConroller(DbLogServiceImpl dbLogService, DbLockServiceImpl dbLockService, DbCreateTestObjectServiceImpl dbCreateTestObjectService, DbTableServiceImpl dbTableService) {
        this.dbLogService = dbLogService;
        this.dbLockService = dbLockService;
        this.dbCreateTestObjectService = dbCreateTestObjectService;
        this.dbTableService = dbTableService;
    }

    @ApiOperation(value = "Запустить очистку таблицы", nickname = "cleaning")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Возвращает ИД запроса"),
            @ApiResponse(code = 500, message = "Ошибка создания задачи"),
    })
    @DeleteMapping(value = "/api/v1/cleaning", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseInfo cleaning(@RequestBody CleaningDto cleaningDto) {
        try {
            Executor executor = new Executor(dbLogService, dbLockService, dbTableService, cleaningDto.getTableName(), Integer.parseInt(cleaningDto.getCountDelRecInOneIteration()), Integer.parseInt(cleaningDto.getMsSleep()), cleaningDto.getWho(), Long.parseLong(cleaningDto.getDayOld()));
            Long lockTaskId = executor.getLockTaskId();

            ResponseInfo responseInfo = new ResponseInfo();
            responseInfo.setId(lockTaskId);
            if (lockTaskId < 0) {
                executor = null;
                responseInfo.setMsg("Таблица " + cleaningDto.getTableName() + " заблокирована для удаления другим процессом");
            } else {
                executor.start();
                responseInfo.setMsg("Задача на очистку таблицы " + cleaningDto.getTableName() + " запущена. УИД задачи " + lockTaskId);
            }
            return responseInfo;
        } catch (Exception e){
            e.printStackTrace();
            throw new InternalServerErrorException(e.getLocalizedMessage(),"unknown","создание_задачи");
        }
    }

    @ApiOperation(value = "Получить состояние задачи по её ИД", nickname = "tasksForId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Возвращает состояние задачи"),
            @ApiResponse(code = 500, message = "Ошибка"),
    })
    @GetMapping(value = "/api/v1/task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DbLock> tasksForId(@ApiParam(value = "ИД-задачи (для получения всех задач используейте all)",defaultValue = "all") @PathVariable(value="id") String id) {
        try{
            List<DbLock> list;
            if (id.equals("all"))
                list= dbLockService.findAll();
            else
                list= dbLockService.findById(Long.parseLong(id));
            this.dbLogService.insertToLogSuccess("просмотр_задач", UUID.randomUUID().toString(),"параметры просмотра (id="+id+")");

            return list;
        }
        catch (Exception e){
            e.printStackTrace();
            throw new InternalServerErrorException(e.getLocalizedMessage(),"unknown","просмотр_задач");
        }
    }

    @ApiOperation(value = "Получить последние 20-ь записей в логах", nickname = "getLogs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Возвращает последние записи в логах"),
            @ApiResponse(code = 500, message = "Ошибка"),
    })
    @GetMapping(value = "/api/v1/log/last20", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DbLog> getLogs(@ApiParam(value = "Сквозной UID события (мб пусто)") @RequestParam(required = false, defaultValue = "",value="eventid") String eventid) {
        try{
            List<DbLog> list;
            if (eventid.length()>0)
                list=this.dbLogService.findLast20EventId(eventid);
            else
                list=this.dbLogService.getLast20();
            this.dbLogService.insertToLogSuccess("запрос_логов", UUID.randomUUID().toString(),"запрос логов по eventid="+eventid);
            return list;
        }
        catch (Exception e){
            e.printStackTrace();
            throw new InternalServerErrorException(e.getLocalizedMessage(),"unknown","запрос_логов");
        }
    }

    @ApiOperation(value = "Пересоздать все тестовые объекты и заполнить их тестовыми данными", nickname = "recreateTestObject")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успех"),
            @ApiResponse(code = 500, message = "Ошибка"),
    })
    @PostMapping(value = "/api/v1/recreateTestObject", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseInfo recreateTestObject(@RequestBody RecreateTestObjectDto recreateTestObjectDto) {
        try{
            dbCreateTestObjectService.start(Integer.parseInt(recreateTestObjectDto.getCountTable()), Integer.parseInt(recreateTestObjectDto.getCountRecord()), Integer.parseInt(recreateTestObjectDto.getMaxDayBack()));
            this.dbLogService.insertToLogSuccess("ген_тест_объектов", UUID.randomUUID().toString(),"параметры генерированя (counttable="+recreateTestObjectDto.getCountTable()+", countrecord="+recreateTestObjectDto.getCountRecord()+", maxdayback="+recreateTestObjectDto.getMaxDayBack()+")");
            ResponseInfo ri=new ResponseInfo();
            ri.setId((long)-1);
            ri.setMsg("тестовые объекты успешно созданы и заполнены тестовыми данными");
            return ri;
        }
        catch (Exception e){
            e.printStackTrace();
            throw new InternalServerErrorException(e.getLocalizedMessage(),"unknown","ген_тест_объектов");
        }
    }

}
