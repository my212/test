package com.example.task2.controllers.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestParam;

@ApiModel(description = "Модель для параметров запуска очистки таблицы")
public class CleaningDto {
    @ApiModelProperty(value = "UID-вызывающего потока", example = "unknown", allowEmptyValue = true, required = false)
    private String who;

    @ApiModelProperty(value = "имя таблицы", example = "table_1", allowEmptyValue = false, required = true)
    private String tableName;

    @ApiModelProperty(value = "какое кол-во дней от текущей даты будет означать, что запись устарела и её необходимо удалить", example = "5", allowEmptyValue = false, required = true)
    private String dayOld;

    @ApiModelProperty(value = "пауза в мс", example = "1000", allowEmptyValue = false, required = true)
    private String msSleep;

    @ApiModelProperty(value = "кол-во записей для удаления в одной итерации", example = "100", allowEmptyValue = false, required = true)
    private String countDelRecInOneIteration;

    public CleaningDto(String who, String tableName, String dayOld, String msSleep, String countDelRecInOneIteration) {
        this.who = who;
        this.tableName = tableName;
        this.dayOld = dayOld;
        this.msSleep = msSleep;
        this.countDelRecInOneIteration = countDelRecInOneIteration;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getDayOld() {
        return dayOld;
    }

    public void setDayOld(String dayOld) {
        this.dayOld = dayOld;
    }

    public String getMsSleep() {
        return msSleep;
    }

    public void setMsSleep(String msSleep) {
        this.msSleep = msSleep;
    }

    public String getCountDelRecInOneIteration() {
        return countDelRecInOneIteration;
    }

    public void setCountDelRecInOneIteration(String countDelRecInOneIteration) {
        this.countDelRecInOneIteration = countDelRecInOneIteration;
    }
}
