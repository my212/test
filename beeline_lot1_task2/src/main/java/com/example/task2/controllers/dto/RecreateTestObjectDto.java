package com.example.task2.controllers.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Модель для параметров запуска создания тестовых объектов")
public class RecreateTestObjectDto {
    @ApiModelProperty(value = "Кол-во таблиц", example = "4", allowEmptyValue = false, required = true)
    private String countTable;

    @ApiModelProperty(value = "Кол-во записей в таблице", example = "5000", allowEmptyValue = false, required = true)
    private String countRecord;

    @ApiModelProperty(value = "Кол-во дней для random", example = "10", allowEmptyValue = false, required = true)
    private String maxDayBack;

    public RecreateTestObjectDto(String countTable, String countRecord, String maxDayBack) {
        this.countTable = countTable;
        this.countRecord = countRecord;
        this.maxDayBack = maxDayBack;
    }

    public String getCountTable() {
        return countTable;
    }

    public void setCountTable(String countTable) {
        this.countTable = countTable;
    }

    public String getCountRecord() {
        return countRecord;
    }

    public void setCountRecord(String countRecord) {
        this.countRecord = countRecord;
    }

    public String getMaxDayBack() {
        return maxDayBack;
    }

    public void setMaxDayBack(String maxDayBack) {
        this.maxDayBack = maxDayBack;
    }
}
