package com.example.task2.controllers.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

@ApiModel(description = "Модель для ответа в результате ошибки")
public class ResponseError {

    @ApiModelProperty(value = "Ид события", example = "9aceb781-51bf-4dd7-aece-6878426021b6", required = false)
    private String eventId;

    @ApiModelProperty(value = "Тип события", example = "", required = false)
    private String type;

    @ApiModelProperty(value = "Код ответа", example = "illegalArgumentException", required = true)
    private String code;

    @ApiModelProperty(value = "Описание ошибки", example = "Значение поля не может бвть пустым", required = true)
    private String message;

    public ResponseError(String eventId,String type, String code, String message) {
        this.eventId = eventId;
        this.type=type;
        this.code = code;
        this.message = message;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

