package com.example.task2.controllers.dto;

/**
 * Класс создан для ответа на запрос пользователя. Позволяет возвращать в ответе сразу две сущности (строка и УИД)
 */
public class ResponseInfo {
    /** текстовое сообщение */
    private String msg;
    /** уникальный идентификатор */
    private Long id;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
