-- FUNCTION: public.deloldrecord(text, text, text)

-- DROP FUNCTION public.deloldrecord(text, text, text);

CREATE OR REPLACE FUNCTION public.deloldrecord(
	tablename text,
	countrecord text,
	oldday text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE

AS $BODY$
DECLARE
	r RECORD;
	i int:=0;
BEGIN

    FOR r IN
		execute ('SELECT * FROM '||tablename||' t
		where t.dt<now() - ('''||oldday||' day'''||')::interval
		limit '||countrecord)
    LOOP
		execute ('delete from '||tablename||' where id='||r.id);
        RAISE NOTICE 'из таблицы % удалена запись: %', tablename, r.id;
		i=i+1;
    END LOOP;
	return i;
END
$BODY$;

-- PROCEDURE: public.inserttestdata(integer, integer, integer)

-- DROP PROCEDURE public.inserttestdata(integer, integer, integer);

CREATE OR REPLACE PROCEDURE public.inserttestdata(
	counttable integer,
	countrecord integer,
	maxdayback integer)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    str TEXT := 'string';
BEGIN
    FOR i IN 1 .. countTable LOOP
        RAISE NOTICE 'создаём таблицу = %', i;
        EXECUTE ('create table table_' || i || '(str character varying COLLATE pg_catalog."default", dt timestamp without time zone, "id" bigint NOT NULL DEFAULT nextval('''||'seq_all_id'''||'::regclass), CONSTRAINT table_' || i ||'_pkey PRIMARY KEY ("id"));');

		FOR j IN 1 .. countrecord LOOP
			EXECUTE ('INSERT INTO table_' || i ||' ("dt","str") VALUES (now() - INTERVAL '''|| floor(random() * maxdayback + 1)::int||' DAY'''||','''||str||j||''');');
		END LOOP;
    END LOOP;

END
$BODY$;

-- PROCEDURE: public.recreate_test_object(integer, integer, integer)

-- DROP PROCEDURE public.recreate_test_object(integer, integer, integer);

CREATE OR REPLACE PROCEDURE public.recreate_test_object(
	counttable integer,
	countrecord integer,
	maxdayback integer)
LANGUAGE 'plpgsql'
AS $BODY$
declare
	c INT;
	r RECORD;
BEGIN
SELECT count(*) FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME = 'lock' INTO c;
if c>0 then
	drop table lock;
end if;

SELECT count(*) FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME = 'logs' INTO c;
if c>0 then
	drop table logs;
end if;
    FOR r IN
        SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME like 'table_%'
    LOOP
        EXECUTE 'drop table ' || r.table_name;
    END LOOP;
IF EXISTS (SELECT 0 FROM pg_class where relname = 'seq_all_id') then
	DROP SEQUENCE seq_all_id;
end if;
CREATE SEQUENCE seq_all_id INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

create table lock
(
    "id" bigint NOT NULL DEFAULT nextval('seq_all_id'::regclass),
    "tablename" character varying COLLATE pg_catalog."default",
    "who" character varying COLLATE pg_catalog."default",
    "dayold" bigint,
    "totalrecordtodelete" bigint,
    "recorddeleted" bigint,
    dtstart timestamp without time zone DEFAULT now(),
    dtupdate timestamp without time zone,
    CONSTRAINT lock_pkey PRIMARY KEY ("id")
);
create table logs
(
    type character varying COLLATE pg_catalog."default",
    eventid character varying COLLATE pg_catalog."default",
    msg character varying COLLATE pg_catalog."default",
    status character varying COLLATE pg_catalog."default",
    dt timestamp without time zone DEFAULT now(),
    id bigint NOT NULL DEFAULT nextval('seq_all_id'::regclass),
    CONSTRAINT logs_pkey PRIMARY KEY (id)
);

CALL public.inserttestdata(counttable, countrecord, maxdayback);

END
$BODY$;
