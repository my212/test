package com.example.task2.db.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
/**
 * Класс-сущность описывающая таблицу активых задач (иначе называемую таблицу залоченных таблиц)
 */
@Entity
@Table(name = "lock")
public class DbLock {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;

    /** Имя таблицы для очистки от старых данных */
    @Column(name = "tablename")
    private String tableName;

    /** Имя или УИД процесса затребоавшего очистку (кто вызвал веб-сервис) */
    @Column(name = "who")
    private String who;

    /** Количество дней при привышении которого от текущей даты данные считаются устаревшими и подлежат удалению */
    @Column(name = "dayold")
    private Long dayOld;

    /** Общее количество записей которое нужно будет удалить из таблицы */
    @Column(name = "totalrecordtodelete")
    private Long totalRecordToDelete;

    /** Количество записей удалённых на данный момент */
    @Column(name = "recorddeleted")
    private Long recordDeleted;

    /** Дата/время запуска задачи на очистку таблицы от старых данных */
    @Column(name = "dtstart")
    private LocalDateTime dtStart;

    /** Дата/время последнего обновления записи в таблице активных задач процессом очистки */
    @Column(name = "dtupdate")
    private LocalDateTime dtUpdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public Long getDayOld() {
        return dayOld;
    }

    public void setDayOld(Long dayOld) {
        this.dayOld = dayOld;
    }

    public Long getTotalRecordToDelete() {
        return totalRecordToDelete;
    }

    public void setTotalRecordToDelete(Long totalRecordToDelete) {
        this.totalRecordToDelete = totalRecordToDelete;
    }

    public Long getRecordDeleted() {
        return recordDeleted;
    }

    public void setRecordDeleted(Long recordDeleted) {
        this.recordDeleted = recordDeleted;
    }

    public LocalDateTime getDtStart() {
        return dtStart;
    }

    public void setDtStart(LocalDateTime dtStart) {
        this.dtStart = dtStart;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

}
