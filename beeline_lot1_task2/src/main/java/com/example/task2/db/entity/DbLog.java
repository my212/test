package com.example.task2.db.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Класс-сущность описывающая таблицу логов в БД
 */
@Entity
@Table(name = "logs")
public class DbLog {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Long id;

    /** тип */
    @Column(name = "type")
    private String type;

    /** сквозной уид */
    @Column(name = "eventid")
    private String eventId;

    /** текст */
    @Column(name = "msg")
    private String msg;

    /** статус Ошибка/успех */
    @Column(name = "status")
    private String status;

    /** дата/время возникновения события */
    @Column(name = "dt")
    private LocalDateTime dt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDt() {
        return dt;
    }

    public String getEventId() {
        return eventId;
    }

    public String getMsg() {
        return msg;
    }

    public String getType() {
        return type;
    }

    public void setDt(LocalDateTime dt) {
        this.dt = dt;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setType(String type) {
        this.type = type;
    }
}
