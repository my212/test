package com.example.task2.db.repository;

import com.example.task2.db.entity.DbLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbCreateTestObjRepository extends JpaRepository<DbLog, String> {
    String  PSQL_STORED_FUN_deloldrecord="-- FUNCTION: public.deloldrecord(text, text, text)\n" +
            "\n" +
            "-- DROP FUNCTION public.deloldrecord(text, text, text);\n" +
            "\n" +
            "CREATE OR REPLACE FUNCTION public.deloldrecord(\n" +
            "\ttablename text,\n" +
            "\tcountrecord text,\n" +
            "\toldday text)\n" +
            "    RETURNS integer\n" +
            "    LANGUAGE 'plpgsql'\n" +
            "\n" +
            "    COST 100\n" +
            "    VOLATILE \n" +
            "    \n" +
            "AS $BODY$\n" +
            "DECLARE\n" +
            "\tr RECORD;\n" +
            "\ti int:=0;\n" +
            "BEGIN\n" +
            "\t\t\n" +
            "    FOR r IN\n" +
            "\t\texecute ('SELECT * FROM '||tablename||' t\n" +
            "\t\twhere t.dt<now() - ('''||oldday||' day'''||')::interval\n" +
            "\t\tlimit '||countrecord)\n" +
            "    LOOP\n" +
            "\t\texecute ('delete from '||tablename||' where id='||r.id);\n" +
            "        RAISE NOTICE 'из таблицы % удалена запись: %', tablename, r.id;\n" +
            "\t\ti=i+1;\n" +
            "    END LOOP;\n" +
            "\treturn i;\n" +
            "END\n" +
            "$BODY$;\n" +
            "\n" +
            "ALTER FUNCTION public.deloldrecord(text, text, text)\n" +
            "    OWNER TO postgres;\n";

    String PSQL_STORED_PROC_inserttestdata = "CREATE OR REPLACE PROCEDURE public.inserttestdata(\n" +
            "\tcounttable integer,\n" +
            "\tcountrecord integer,\n" +
            "\tmaxdayback integer)\n" +
            "LANGUAGE 'plpgsql'\n" +
            "AS $BODY$\n" +
            "DECLARE\n" +
            "    str TEXT := 'string';\n" +
            "BEGIN\n" +
            "    FOR i IN 1 .. countTable LOOP \n" +
            "        RAISE NOTICE 'создаём таблицу = %', i;\n" +
            "        EXECUTE ('create table table_' || i || '(str character varying COLLATE pg_catalog.\"default\", dt timestamp without time zone, \"id\" bigint NOT NULL DEFAULT nextval('''||'seq_all_id'''||'::regclass), CONSTRAINT table_' || i ||'_pkey PRIMARY KEY (\"id\"));');\n" +
            "\t\t\n" +
            "\t\tFOR j IN 1 .. countrecord LOOP\t\n" +
            "\t\t\tEXECUTE ('INSERT INTO table_' || i ||' (\"dt\",\"str\") VALUES (now() - INTERVAL '''|| floor(random() * maxdayback + 1)::int||' DAY'''||','''||str||j||''');');\n" +
            "\t\tEND LOOP;\n" +
            "    END LOOP;\n" +
            "\n" +
            "END\n" +
            "$BODY$;\n";

    String PSQL_STORED_PROC_recreate_test_object ="CREATE OR REPLACE PROCEDURE public.recreate_test_object(\n" +
            "\tcounttable integer,\n" +
            "\tcountrecord integer,\n" +
            "\tmaxdayback integer)\n" +
            "LANGUAGE 'plpgsql'\n" +
            "AS $BODY$\n" +
            "declare\n" +
            "\tc INT;\n" +
            "\tr RECORD;\n" +
            "BEGIN\n" +
            "SELECT count(*) FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME = 'lock' INTO c;\n" +
            "if c>0 then\n" +
            "\tdrop table lock;\n" +
            "end if;\n" +
            "\n" +
            "SELECT count(*) FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME = 'logs' INTO c;\n" +
            "if c>0 then\n" +
            "\tdrop table logs;\n" +
            "end if;\t\n" +
            "    FOR r IN\n" +
            "        SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='BASE TABLE' and t.TABLE_NAME like 'table_%'\n" +
            "    LOOP\n" +
            "        EXECUTE 'drop table ' || r.table_name;\n" +
            "    END LOOP;\n" +
            "IF EXISTS (SELECT 0 FROM pg_class where relname = 'seq_all_id') then \n"+
            "\tDROP SEQUENCE seq_all_id;\n" +
            "end if;\n" +
            "CREATE SEQUENCE seq_all_id INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1; \n"+
            "\n" +
            "create table lock\n" +
            "(\n" +
            "    \"id\" bigint NOT NULL DEFAULT nextval('seq_all_id'::regclass),\n" +
            "    \"tablename\" character varying COLLATE pg_catalog.\"default\",\n" +
            "    \"who\" character varying COLLATE pg_catalog.\"default\",\n" +
            "    \"dayold\" bigint,\n" +
            "    \"totalrecordtodelete\" bigint,\n" +
            "    \"recorddeleted\" bigint,\n" +
            "    dtstart timestamp without time zone DEFAULT now(),\n" +
            "    dtupdate timestamp without time zone,\n" +
            "    CONSTRAINT lock_pkey PRIMARY KEY (\"id\")\n" +
            ");\n" +
            "create table logs\n" +
            "(\n" +
            "    type character varying COLLATE pg_catalog.\"default\",\n" +
            "    eventid character varying COLLATE pg_catalog.\"default\",\n" +
            "    msg character varying COLLATE pg_catalog.\"default\",\n" +
            "    status character varying COLLATE pg_catalog.\"default\",\n" +
            "    dt timestamp without time zone DEFAULT now(),\n" +
            "    id bigint NOT NULL DEFAULT nextval('seq_all_id'::regclass),\n" +
            "    CONSTRAINT logs_pkey PRIMARY KEY (id)\n" +
            ");\t\t\t \n" +
            "\n" +
            "CALL public.inserttestdata(counttable, countrecord, maxdayback);\n" +
            "\n" +
            "END\n" +
            "$BODY$;\n";
}
