package com.example.task2.db.repository;
import com.example.task2.db.entity.DbLock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbLockRepository extends JpaRepository<DbLock, Long> {
    long countByTableName(String name);
}
