package com.example.task2.db.repository;

import com.example.task2.db.entity.DbLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DbLogRepository extends JpaRepository<DbLog, Long> {
    @Query(nativeQuery=true, value = "SELECT * FROM logs t order by t.dt desc LIMIT 20")
    List<DbLog> findLast20();

    @Query(nativeQuery=true, value = "SELECT * FROM logs t where t.eventid=?1 order by t.dt desc LIMIT 20")
    List<DbLog> findLast20EventId(String eventid);
}
