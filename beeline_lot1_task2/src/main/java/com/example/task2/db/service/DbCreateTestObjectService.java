package com.example.task2.db.service;

public interface DbCreateTestObjectService {
    /**
     * Создаёт необходимые объекты БД и заполяет их тестовыми данными
     * @param countTable - сколько тестовых таблиц создавать
     * @param countRecord - сколько записей в каждой тестовой таблице создавать
     * @param maxDayBack - максимальное количество дней назад от текущей даты когда запись в тестовой таблице могла быть создана
     */
    public void start(int countTable, int countRecord, int maxDayBack);
}
