package com.example.task2.db.service;

import com.example.task2.db.repository.DbCreateTestObjRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Класс-сервис для работы с БД, создания необходимых объектов БД и заполнения их тестовыми данными
 */
@Service
@Transactional
public class DbCreateTestObjectServiceImpl implements DbCreateTestObjectService {

    private final DbCreateTestObjRepository dbCreateTestObjRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DbCreateTestObjectServiceImpl(DbCreateTestObjRepository dbCreateTestObjRepository) {
        this.dbCreateTestObjRepository = dbCreateTestObjRepository;
    }

    public void start(int countTable, int countRecord, int maxDayBack) {
        jdbcTemplate.execute(dbCreateTestObjRepository.PSQL_STORED_FUN_deloldrecord);
        jdbcTemplate.execute(dbCreateTestObjRepository.PSQL_STORED_PROC_inserttestdata);
        jdbcTemplate.execute(dbCreateTestObjRepository.PSQL_STORED_PROC_recreate_test_object);
        jdbcTemplate.execute("CALL public.recreate_test_object("+countTable+", "+countRecord+", "+maxDayBack+")");
    }
}
