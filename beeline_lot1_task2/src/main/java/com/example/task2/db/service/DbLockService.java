package com.example.task2.db.service;

import com.example.task2.db.entity.DbLock;

import java.util.List;

public interface DbLockService {
    /**
     * Получить список из одной задачи с указанным УИД-ом
     * @param id - УИД искомой задачи
     * @return возвращает список из одной (или ни одной) задачи с данным УиД-ом
     */
    public List<DbLock> findById(Long id);

    /**
     * Проверить заблокирована ли данная таблица по её имени другим процессом очистки или нет
     * @param tableName - имя таблицы для очистки
     * @return возвращает истину - если таблица заблокирована и ложь - если таблица свободна и на ней может быть запущен процесс очистки
     */
    public boolean isLockTable(String tableName);

    /**
     * Поиск всех активных задач очистки таблиц
     * @return возвращает список из найденных активных задач
     */
    public List<DbLock> findAll();

    /**
     * Удалить задачу из таблицы активных задач по её УИД-у
     * @param id - УИД-задачи
     */
    public void delete(Long id);

    /**
     * Обновить параметры активной задачи по очистке в таблице активных задач
     * @param id - УИД-задачи
     * @param countDeleted - Кол-во удалённых записей из таблицы на данный момоент
     */
    public void update(Long id, Integer countDeleted);

    /**
     * Создать в таблице активных задач запись о новой активной задаче
     * @param tablename - имя таблицы для очистки
     * @param who - кто запустил процесс (кто вызвал веб-сервис)
     * @param dayold - кол-во дней от текущей даты начиная с которых запись в очищаемой таблице считается устаревшей и подлежавшей удалению
     * @return возвращает УИД созданной задачи
     */
    public Long create(String tablename, String who, Long dayold);
}
