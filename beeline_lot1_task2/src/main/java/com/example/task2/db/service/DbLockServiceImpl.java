package com.example.task2.db.service;
import com.example.task2.db.entity.DbLock;
import com.example.task2.db.repository.DbLockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс-сервис для работы с таблицей активных задач
 */
@Service
@Primary
@Transactional
public class DbLockServiceImpl implements DbLockService {
    private final DbLockRepository dbLockRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public DbLockServiceImpl(DbLockRepository dbLockRepository) {
        this.dbLockRepository = dbLockRepository;
    }

    public List<DbLock> findById(Long id){
        List<DbLock> list = new ArrayList<DbLock>();
        list.add(dbLockRepository.findById(id).orElse(null));
        return list;
    }

    public boolean isLockTable(String tableName){
        return dbLockRepository.countByTableName(tableName) > 0;
    }


    public List<DbLock> findAll(){
        return dbLockRepository.findAll();
    }

    public void delete(Long id){
        dbLockRepository.deleteById(id);
    }

    public void update(Long id, Integer countDeleted){
        DbLock dbLock = dbLockRepository.getOne(id);
        dbLock.setDtUpdate(LocalDateTime.now());
        dbLock.setRecordDeleted(dbLock.getRecordDeleted()+countDeleted);
        dbLockRepository.save(dbLock);
    }

    public Long create(String tablename, String who, Long dayold){
        DbLock dbLock = new DbLock();
        dbLock.setDayOld(dayold);
        dbLock.setTableName(tablename);
        dbLock.setWho(who);
        dbLock.setRecordDeleted((long)0);
        dbLock.setTotalRecordToDelete((long)this.findCountRecordForDelOnTable(tablename,dayold.toString()));
        dbLock.setDtStart(LocalDateTime.now());
        dbLockRepository.save(dbLock);
        return dbLock.getId();
    }

    /**
     * Функция подсчёта общего числа записей в таблице подлежащих удалению
     * @param tablename - имя таблицы для очистки
     * @param daymax - кол-во дней от текущей даты начиная с которых запись в очищаемой таблице считается устаревшей и подлежавшей удалению
     * @return общее число записей подлежащих удалению
     */
    private Integer findCountRecordForDelOnTable(String tablename, String daymax) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM "+tablename+" t where t.dt<now() - INTERVAL '"+daymax+" DAY'", Integer.class);
    }
}
