package com.example.task2.db.service;

import com.example.task2.db.entity.DbLog;

import java.util.List;

public interface DbLogService {
    /**
     * Получить список из 20-и последних записей в логах
     * @return возвращает список из 20-и последних записей в логах
     */
    public List<DbLog> getLast20();

    /**
     * Получить список из 20-и последних записей в логах по сквозному ид-события
     * @param eventid - сквозное ид-события
     * @return озвращает список из 20-и последних записей в логах
     */
    public List<DbLog> findLast20EventId(String eventid);

    /**
     * Метод создающий запись об успешном событии
     * @param type - тип события
     * @param eventid - сквозное ид-события
     * @param msg - текстовое сообщение
     */
    public default void insertToLogSuccess(String type, String eventid, String msg) {
        this.insertToLog(type,eventid,msg,"OK");
    }

    /**
     * Метод создающий запись об ошибочном событии
     * @param type - тип события
     * @param eventid - сквозное ид-события
     * @param msg - текстовое сообщение
     */
    public default void insertToLogError(String type, String eventid, String msg) {
        this.insertToLog(type,eventid,msg,"ERROR");
    }

    /**
     * Метод создающий запись о событии
     * @param type - тип события
     * @param eventid - сквозное ид-события
     * @param msg - текстовое сообщение
     * @param status - статус события
     */
    public void insertToLog(String type, String eventid, String msg, String status);
}
