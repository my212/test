package com.example.task2.db.service;

import com.example.task2.db.entity.DbLog;
import com.example.task2.db.repository.DbLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Класс-сервис для работы с таблицей логов в БД
 */
@Service
@Primary
@Transactional
public class DbLogServiceImpl implements DbLogService {
    private final DbLogRepository dbLogRepository;

    @Autowired
    public DbLogServiceImpl(DbLogRepository dbLogRepository) {
        this.dbLogRepository = dbLogRepository;
    }

    public List<DbLog> getLast20(){
        return dbLogRepository.findLast20(); //.findAll();
    }

    public List<DbLog> findLast20EventId(String eventid){
        return dbLogRepository.findLast20EventId(eventid);
    }

    public void insertToLog(String type, String eventid, String msg, String status){
        try {
            DbLog dbLog = new DbLog();
            dbLog.setType(type);
            dbLog.setEventId(eventid);
            dbLog.setMsg(msg);
            dbLog.setStatus(status);
            dbLog.setDt(LocalDateTime.now());
            dbLogRepository.save(dbLog);
            //System.out.println("Log record uid: "+dbLog.getId());
        }catch (Exception e){e.printStackTrace();}
    }
}
