package com.example.task2.db.service;

public interface DbTableService {
    /**
     * Вызвать хранимую процедуру очистки таблицы с параметрами
     * @param tableName - имя очищаемой таблицы
     * @param countRecord - сколько устаревших записей можно удалить за один вызов процедуру
     * @param oldDay - кол-во дней от текущей даты начиная с которых запись в очищаемой таблице считается устаревшей и подлежавшей удалению
     * @return озвращает количество фактически удалнных хранимой процедурой строк
     */
    public Integer callDelRec(String tableName, String countRecord, String oldDay);
}
