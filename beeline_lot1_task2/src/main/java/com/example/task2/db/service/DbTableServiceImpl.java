package com.example.task2.db.service;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;

/**
 * Класс-сервис для работы с очищаемой таблицей
 */
@Service
@Transactional
public class DbTableServiceImpl implements DbTableService {
    @PersistenceContext
    EntityManager entityManager;

    public Integer callDelRec(String tableName, String countRecord, String oldDay){
        String queryString = "SELECT deloldrecord('"+tableName+"', '"+countRecord+"', '"+oldDay+"')";
        Query query = entityManager.createNativeQuery(queryString);
        Object result = query.getSingleResult();
        Integer i=(Integer)result;
        return i;
    }

}
