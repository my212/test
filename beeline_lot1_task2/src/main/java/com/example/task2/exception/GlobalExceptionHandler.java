package com.example.task2.exception;

import com.example.task2.controllers.dto.ResponseError;
import com.example.task2.db.service.DbLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Глобальный обработчик исключений
 */
@RestControllerAdvice(basePackages = "com.example.task2.controllers")
public class GlobalExceptionHandler {

    //@Value("${spring.application.name}")
    //private String system;

    private final DbLogService dbLogService;

    @Autowired
    public GlobalExceptionHandler(DbLogService dbLogService) {
        this.dbLogService = dbLogService;
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<ResponseError> InternalServerErrorException(InternalServerErrorException exception) {
        ResponseError error = new ResponseError(
                exception.getEventId(),
                exception.getType(),
                "InternalServerErrorException",
                exception.getLocalizedMessage()
        );
        System.err.println(exception.getLocalizedMessage());
        this.dbLogService.insertToLogError(exception.getType(), exception.getEventId(),exception.getLocalizedMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

