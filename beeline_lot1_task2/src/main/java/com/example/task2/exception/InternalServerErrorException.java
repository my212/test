package com.example.task2.exception;
/**
 * Пользовательское исключение: ошибка на стороне сервера
 */
public class InternalServerErrorException  extends RuntimeException {
    /** Сквозной уид-события */
    private String eventId;

    /** Тип события */
    private String type;

    public InternalServerErrorException(String message, String eventId, String type) {
        super(message);
        this.eventId=eventId;
        this.type=type;
    }

    public String getEventId() {
        return eventId;
    }

    public String getType() {
        return type;
    }
}
