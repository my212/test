package com.example.task2.exec;

import com.example.task2.db.service.*;
import com.example.task2.exception.InternalServerErrorException;

import java.util.UUID;

/**
 * Класс-поток выполняющий задачу очистки одной таблицы
 */
public class Executor extends Thread {
    private final DbLogService dbLogService;
    private final DbLockService dbLockService;
    private final DbTableServiceImpl dbTableService;

    /** УИД задачи очистки*/
    private Long lockTaskId;
    /** Кол-во удаляемых записей в одной итерации */
    private Integer coutDelRecInOneIteration;
    /** Пауза между итерациями в мс */
    private Integer msSleep;
    /** Имя очищаемой таблицы */
    private String tableName;
    /** Ид-сквозного события для записи в логи*/
    private String myUID;
    /** Кто запустил задачу очистки таблицы (кто вызвал веб-сервис) */
    private String who;
    /** Кол-во дней от тек даты начиная с которого запись в таблице считается устаревшей и подлежащей удалению */
    private Long dayold;

    public Executor(DbLogService dbLogService, DbLockService dbLockService, DbTableServiceImpl dbTableService, String tableName, Integer coutDelRecInOneIteration, Integer msSleep, String who, Long dayold) {
        super(tableName);

        this.dbLogService = dbLogService;
        this.dbLockService = dbLockService;
        this.dbTableService=dbTableService;

        this.coutDelRecInOneIteration=coutDelRecInOneIteration;
        this.msSleep=msSleep;
        this.tableName=tableName;
        this.who=who;
        this.dayold=dayold;

        myUID= UUID.randomUUID().toString();
        this.dbLogService.insertToLogSuccess("удалить_зап_из_табл", myUID,"запрос на удаление записей (tableName="+tableName+", msSleep="+msSleep+", coutDelRecInOneIteration="+coutDelRecInOneIteration+")");
    }

    /**
     * Функция создания новой задачи в таблице активных задач и получения её УИД-а
     * @return возвращает УИД созданноё задачи. Или -1 если над таблицей работает другой процесс очистки
     */
    public Long getLockTaskId(){
        boolean b=dbLockService.isLockTable(this.tableName);
        if (b==true) {
            this.dbLogService.insertToLogError("удалить_зап_из_табл", myUID,"таблица заблокирована другим процессом (уже идёт удаление старых записей)");
            return (long)-1;
        }
        else{
            lockTaskId=dbLockService.create(tableName, who, dayold);
            this.dbLogService.insertToLogSuccess("удалить_зап_из_табл", myUID,"создана задача по  очистке таблицы от старых данных id="+lockTaskId);
            return lockTaskId;
        }
    }

    /**
     * Запуск очистки таблицы в виде отдельного процеса
     */
    public void run(){
        System.out.printf("%s clean started... \n", Thread.currentThread().getName());
        try{
            while(true) {
                Thread.sleep(this.msSleep);
                Integer countDeleted = dbTableService.callDelRec(tableName, coutDelRecInOneIteration.toString(), dayold.toString());
                this.dbLogService.insertToLogSuccess("удалить_зап_из_табл", myUID, "выполнена итерация. Удалено " + countDeleted);
                if (countDeleted < coutDelRecInOneIteration) break;
                dbLockService.update(this.lockTaskId, countDeleted);
            }
        }
        catch(InterruptedException e){
            System.out.println("Thread "+Thread.currentThread().getName()+" has been interrupted");
            dbLockService.delete(this.lockTaskId);
            throw new InternalServerErrorException("очистка таблицы провалилась с ошибкой: "+e.getLocalizedMessage(),myUID,"удалить_зап_из_табл");
        }
        System.out.printf("%s clean fiished... \n", Thread.currentThread().getName());
        dbLockService.delete(this.lockTaskId);
        this.dbLogService.insertToLogSuccess("удалить_зап_из_табл", myUID,"очистка таблицы успешно завершена");
    }

}
